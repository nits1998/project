import React, { Component } from "react";
class ThanksPage extends Component{
    state={};
    
    render(){
        return (
            <div className="container-fluid">
                <h4 className="text-center">Thank You</h4>
                <div className="row">
                <span className="text-center fw-bold">
                    We recieved your order and will process it within next 24 hours!
                </span>
                </div>
            </div>
        );
    }
}
export default ThanksPage;